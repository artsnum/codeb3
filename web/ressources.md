# HTML,CSS Ressources

+ https://freefrontend.com/css-calendars/
+ CSS Grid layout : [https://la-cascade.io/css-grid-layout-guide-complet/](https://la-cascade.io/css-grid-layout-guide-complet/)
+ CSS FRancine : [https://www.vox.com/2018/5/3/17309078/digital-art-diana-a-smith-francine-coded-browser-art?fbclid=IwAR0CwiKGS9s6o7C0jldKwQ_KUHSEBS7VJUeZE0vrlahhXjT84HGdM4aDL9c](https://www.vox.com/2018/5/3/17309078/digital-art-diana-a-smith-francine-coded-browser-art?fbclid=IwAR0CwiKGS9s6o7C0jldKwQ_KUHSEBS7VJUeZE0vrlahhXjT84HGdM4aDL9c)
+ LOREM IPSUM generators : [https://loremipsum.io/fr/ultimate-list-of-lorem-ipsum-generators/](https://loremipsum.io/fr/ultimate-list-of-lorem-ipsum-generators/)
+ Animation: https://github.com/airbnb/lottie-web
+ Basic templates (to share a project..) : [https://html5up.net/](https://html5up.net/)

# DOM Manipulation ressources

+ DOM manipulation with P5js : [https://rainbowsprinklecode.wordpress.com/2017/03/02/html-css-and-the-dom-with-p5-js/](https://rainbowsprinklecode.wordpress.com/2017/03/02/html-css-and-the-dom-with-p5-js/)
+ Basic interaction : [https://editor.p5js.org/jps723/sketches/HJyPPJc3W](https://editor.p5js.org/jps723/sketches/HJyPPJc3W)
+ https://editor.p5js.org/jps723/sketches/Bk_E-k5hZ
+ Input and button: [https://p5js.org/examples/dom-input-and-button.html](https://p5js.org/examples/dom-input-and-button.html)
+ Playing with pixels : [http://playingwithpixels.gildasp.fr/](http://playingwithpixels.gildasp.fr/)
+ Net related art : [https://net-art.org/?fbclid=IwAR1_bmTcZK8rB07vlSAHX_lSykCyMEk2M3XE04_miFVyi9PpWll-VW5Xdxo](https://net-art.org/?fbclid=IwAR1_bmTcZK8rB07vlSAHX_lSykCyMEk2M3XE04_miFVyi9PpWll-VW5Xdxo)
+ A Practical Guide to SVGs on the web : [https://svgontheweb.com/](https://svgontheweb.com/)
+ [Don't fear the Internet](http://dontfeartheinternet.com/)

# CSS

+ CSS Flexbox [https://la-cascade.io/flexbox-guide-complet/](https://la-cascade.io/flexbox-guide-complet/)
+ [http://netart.rocks/notes/cssoverview](http://netart.rocks/notes/cssoverview)
+ [Chat box](https://codepen.io/supah/pen/jqOBqp)
+ [SASS style guide](https://css-tricks.com/sass-style-guide/)
+ Sketch App course: [https://learnux.io/course/sketch/style-overrides](https://learnux.io/course/sketch/style-overrides)

# Works

+ Net Art anthology : https://anthology.rhizome.org/
+ DESSIN: http://formafluens.io/
+ Lightning : https://codepen.io/akm2/full/Aatbf
+ 