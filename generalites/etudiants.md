---
title: Travaux d'étudiant.e.s
lang: fr-FR
---

::: tip
Quelques travaux d'étudiants qui ont utilisé le code, ou auraient pu utiliser.
:::

# Etudiants {#custom-id}

## Noa (Peinture)

## Lucie (Espace urbain)

## Aymeraude (Dessin)

## Francesco (Gravure)

## Design textile

## Com graph

## Animation

