# Raspberry ressources

+ [https://pimylifeup.com/category/projects/beginner/?fbclid=IwAR1X22JtqGnnDMpclQjxB-JqiPkee6boII40hdVF_LmPg2P6z4ISIcxDeS8](https://pimylifeup.com/category/projects/beginner/?fbclid=IwAR1X22JtqGnnDMpclQjxB-JqiPkee6boII40hdVF_LmPg2P6z4ISIcxDeS8)
+ GPIO Header guide : [https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/](https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/)
+ Newbie introduction: [https://www.youtube.com/watch?v=U7bZWWlqrCo&feature=youtu.be&fbclid=IwAR25ZP0keueL--klASLTH1viwHEFiuzKX20XkZQxXhtCVuA-ZZp4HRAMhNs](https://www.youtube.com/watch?v=U7bZWWlqrCo&feature=youtu.be&fbclid=IwAR25ZP0keueL--klASLTH1viwHEFiuzKX20XkZQxXhtCVuA-ZZp4HRAMhNs)

# Arduino

+ [Arduino to Processing notes](http://web.arch.virginia.edu/arch5424/workshops/workshops2017/arduinoToProcessing/arduinoToProcessing.html)
+ Processing, Arduino, Wiring : [http://www.ecole-art-aix.fr/-Processing-](http://www.ecole-art-aix.fr/-Processing-)