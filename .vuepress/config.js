module.exports = {
    title: 'Code B3',
    description: 'Support en ligne pour le CASO Code B3',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: '/assets/styles.css' }]
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Digitalab', link: 'http://digitalab.be' }
        ],
        sidebarDepth: 0,
        sidebar: [
            {
              title: 'Généralités',
              collapsable: true,
              children: [
                '/',
                'generalites/filiation',
                'generalites/inspirations',
                'generalites/etudiants',
                'generalites/scratch',
                'generalites/processing-et-p5js',
                'generalites/monde-physique',
                'generalites/adresses',
                'generalites/outils'
              ]
            },
            {
              title: 'Processing',
              collapsable: true,
              children: [
                'processing/structure',
                'processing/coordonnees',
                'processing/formes-simples',
                'processing/couleur',
                'processing/transformations',
                'processing/variables',
                'processing/conditions',
                'processing/boucles',
                'processing/fonctions',
                'processing/aleatoire',
              ]
            },
            {
              title: 'P5js',
              collapsable: true,
              children: [      
                'p5js/about',
                'p5js/editeur',
                'p5js/mise-en-place',
                'p5js/p5-processing',
                'p5js/ressources',

              ]
            },
            {
              title: 'Projets P5js',
              collapsable: true,
              children: [
                'p5js/projets/interface',

              ]
            },
            {
              title: 'En pratique',
              collapsable: true,
              children: [
                'en-pratique/animation',
                'en-pratique/images',
                'en-pratique/pixels',
                'en-pratique/texte',
                'en-pratique/temps',
                'en-pratique/film',
                'en-pratique/donnees',
                'en-pratique/exporter'
              ]
            },
            {
              title: 'Exercices',
              collapsable: true,
              children: [
                'exercices/formes-simples',
                'exercices/generatif',
                'exercices/animation',
                'exercices/interactivite',
                'exercices/outil-de-dessin',
                'exercices/trames',
                'exercices/horloge',
                'exercices/slitscan'
              ]
            },
            {
              title: 'Pour clôturer',
              collapsable: true,
              children: [
                'cloture/rendus',
                'cloture/formulaire'
              ]
            }/*,
            {
              title: 'Design',
              children: [
                'design/ressources'
              ]
            },
            {
              title: 'HTML & CSS',
              children: [
                'web/ressources'
              ]
            },
            {
              title: 'Data',
              children: [
                'data/ressources'
              ]
            },
            {
              title: 'WebGL',
              children: [
                'data/ressources'
              ]
            },
            {
              title: 'Raspberry PI',
              children: [
                'raspberrypi/ressources'
              ]
            },
            {
              title: 'NodeJS et Express',
              children: [
                'node/ressources'
              ]
            },
            {
              title: 'Satellites',
              children: [
                'satellites/ressources'
              ]
            }*/
            
          ]
    }
}