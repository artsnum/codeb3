---
title: Introduction
lang: fr-FR
---

>"Drawing is a verb"
> [Richard Serra](https://www.moma.org/explore/inside_out/2011/10/20/to-collect/)

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/74725118?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>



## Le code

Le code est un *outil* avec lequel nous pouvons à la fois **créer des choses, ou créer d'autres outils avec lesquels créer des choses**.

Le workshop est organisé sur une journée et vise à **vous familiariser** avec le code à travers une série d'explorations visuelles.

## Processing et P5js

Cette première approche du code, laboratoire et expérimentale, se fera à l'aide de [Processing](https://www.processing.org/), logiciel libre, créé par des artistes, pour des artistes, dans les champs pédagogique et visuel. 
Très visuel lui-même, Processing permet d'entrer facilement dans les **logiques d'un algorithme** et d'en comprendre les fonctions de base (variables, boucles, conditions, fonctions, listes..)

Nous pourrons également utiliser [P5js](https://p5js.org/), bibliothèque javaScript en filiation directe avec Processing, et qui nous permettra d'interagir avec la page web et de partager nos travaux plus facilement.

> Plus d'info sur [Processing & P5js](part0/processing-et-p5js.md)

-----

Notre souhait est que cette journée puisse être, pour tous, une porte ouverte vers de **nouvelles possibilités**, et puisse enrichir votre travail et votre démarche personnelle, vous amenant à une **plus grande autonomie**, à la fois technique et artistique.

Il est important de noter que **ce workshop s'adresse à tous les étudiants de B1**, quelque soient leur option et leur niveau de connaissance en la matière. Il a été conçu comme tel.

<a data-fancybox title="" href="/assets/aymeraude.jpeg">![](/assets/aymeraude.jpeg)</a>
<span class="legende">Le travail d'Aymeraude, option Dessin, entièrement réalisé dans Illustrator.</span>
