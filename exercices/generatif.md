---
title: Générez des formes
lang: fr-FR
---

# Générez des formes

<a data-fancybox title="" href="https://sdl-assets.ams3.cdn.digitaloceanspaces.com/2018/09/sdl-web-venice-bienalle-01-2000x1210.gif">![](https://sdl-assets.ams3.cdn.digitaloceanspaces.com/2018/09/sdl-web-venice-bienalle-01-2000x1210.gif)</a>

Sur base de votre premier agencement de formes simples, créez un programme qui génère à chaque boucle un autre agencement aléatoire \(à l'aide des variables et de fonctions comme [random](https://processing.org/reference/random.html) par exemple.

Chaque item doit être sauvé au format PNG. Veillez à générer des noms de fichiers différents à chaque boucle. Dans le cas contraire, Processing écrasera systématiquement vos anciens fichiers avec le dernier.
La variable **frameCount** est très pratique pour cela. Elle sera automatiquement augmentée de 1, à chaque boucle. Elle peut donc être utilisée comme une sorte de compteur dans les noms de vos fichiers.

## Notions mises en oeuvre

* noLoop() ou tout sans le Setup (pour la première partie)
* Canevas et coordonnées
* Formes simples
* Couleur (remplissage et contours)
* Export PNG
* Eventuellement [transformations](https://fr.flossmanuals.net/processing/les-transformations/)

<a data-fancybox title="" href="/assets/pape01.png">![](/assets/pape01.png)</a>

<a data-fancybox title="" href="/assets/pape2.png">![](/assets/pape2.png)</a>
[Lygia Pape, Livro do tempo](http://www.from-paris.com/lygia-pape-livro-do-tempo-serpentine-gallery-expo/), 1961-63

## Fonctions principales

[Référence](https://processing.org:8443/reference/)


## Exemple

```processing
void setup() {
    size(800,500);
    background(255);
    noLoop();
    noStroke();
}

void draw() {
    //Carrés rouges
    fill(255,0,0); // fill(rouge, vert, bleu)
    rect(0, 0, 100,100);
    rect(width-100, 0, 100,100);
    rect(width-100, height-100, 100,100);
    rect(0, height-100, 100,100);

    //Rectangles noirs
    fill(0);
    rect(200,0,400,100);
    rect(200,height-100,400,100);

    //Triangles jaunes
    fill(250,230,0);
    triangle(200, 100, 600, 100, 600, 300);
    triangle(200, height-100, 200, height-300, 600, height-100);
}
```

<a data-fancybox title="" href="/assets/flag.png">![](/assets/flag.png)</a>

## Références

* [Stockholm Media Lab, 53rd Venice Biennale](http://www.stockholmdesignlab.se/venice-biennale/)
* [Lygia Pape, Livro do tempo](https://s-media-cache-ak0.pinimg.com/originals/d6/10/86/d610860dd92f473c5bc702959bc386f8.jpg)
* [Mickey](http://www.damienhirst.com/mickey), Damien Hirst

----

<a data-fancybox title="" href="/assets/mickey.png">![](/assets/mickey.png)</a>



