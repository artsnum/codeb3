---
title: Animer des éléments
lang: fr-FR
---

# Animer des éléments

```processing
int posX;
int diametre = 100;

void setup() {
    size(800,300);
    background(255);
    noStroke();
    posX = -(diametre/2);
    //frameRate(2);
}
void draw() {
    background(255);
    fill(255,0,0);
    ellipse(posX,mouseY,diametre,diametre);
    posX+=10;
    if(posX>width+(diametre/2)) {
        posX = -(diametre/2);
    }
}
```

<a data-fancybox title="" href="/assets/anim.png">![](/assets/anim.png)</a>

# Le même code, commenté:

```processing
int posX; //Je déclare une variable pour la position en X
int diametre = 100; //Je déclare une variable pour le diamètre

void setup() {
    size(800,300);
    background(255);
    noStroke(); //Je supprime les contours
    // J'assigne une valeur à posX (pour que mon cercle soit hors de mon format au départ...)
    posX = -(diametre/2);
    // Ci-dessous, je modifie éventuellement le nombre de frames par seconde (décommentez si nécessaire)
    //frameRate(2);
}
void draw() {
    background(255);
    // je rempli de rouge
    fill(255,0,0);
    ellipse(posX,mouseY,diametre,diametre);
    //J'incrémente posX de 10, par exemple
    posX+=10;
    // Quand mon cercle est sorti de mon format, à droite, je le fais revenir par la gauche...
    if(posX>width+(diametre/2)) {
        posX = -(diametre/2);
    }
}
```