---
title: La vidéo
lang: fr-FR
---

# La vidéo

Processing permet de créer une vidéo directement depuis son interface à partir d’une séquence d’images générées depuis un sketch. Pour cela, nous allons conjointement utiliser la fonction saveFrame() de Processing et l’outil MovieMaker, accessible depuis le menu Outils > Movie Maker.

### Sauvegarde d’une séquence d’images
La première étape va consister à sauvegarder une série d’images à partir de notre programme. Nous allons utiliser la fonction saveFrame() qui permet d’exporter au format de notre choix (jpeg, png, tiff) l’image produite par notre code. Placée à la fin de la boucle de dessin draw(), elle va permettre de sauver chaque étape de notre animation.

L'utilisation basique de cette fonction (sans paramètres) permet d’avoir un mécanisme qui nomme automatiquement les images exportées sur le disque en fonction du frameCount, variable qui représente le nombre de fois qu’a été appelée la méthode draw() depuis le démarrage de Processing. Les images sont sauvegardées au format .tif et directement écrites dans votre dossier de sketch, à côté du fichier pde.

Attention, saveFrame() étant une opération relativement lourde, il est conseillé de trouver un bon compromis entre la taille du sketch et la fluidité souhaitée de l’animation pendant le rendu.

Pour cet exemple, nous allons écrire un outil de dessin très simple.

```processing
void setup(){
    size(400,400);
}

void draw() {
    ellipse(mouseX,mouseY,60,60);
    saveFrame();
}

```

## L’outil Movie Maker

Si vous regardez votre dossier de sketch après avoir lancé le sketch, vous allez voir que la fonction saveFrame() a créé les fichiers export-0001.tif, export-0002.tif, etc …

<a data-fancybox title="" href="https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_sketch_folder.png">![](https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_sketch_folder.png)</a>

À partir de cette séquence, nous allons générer un fichier vidéo au format Quicktime. Pour cela, nous allons ouvrir l’outil Movie Maker, accessible puis le menu Tools > Movie Maker de Processing.

<a data-fancybox title="" href="https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_interface.png">![](https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_interface.png)</a>

Pour indiquer à Processing quelle séquence d’images il doit assembler, nous allons utiliser le premier sélecteur de fichier en le faisant pointer sur le dossier qui contient les images, qui sera en général le dossier de votre sketch.

<a data-fancybox title="" href="https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_interface_selector.png">![](https://fr.flossmanuals.net/processing/la-video/static/v2_Processing_sortir_la_video_moviemaker_interface_selector.png)</a>

Une fois cette opération effectuée, il est possible de configurer les paramètres de notre vidéo :

+ sa taille par les paramètres **width** et **height** qui peuvent être différents de la taille originelle de vos images.
+ le nombre d’images par seconde ou **Frame Rate** qui va influer sur la fluidité de la vidéo et sa durée finale.
+ le type de **Compression** qui influe sur la qualité finale de la vidéo et de manière implicite la taille en Mo sur votre disque.
+ L’option **«Same size as originals»** permet de caler la largeur et la hauteur de votre vidéo à la taille des images sauvegardées.
Enfin, vous pouvez associer à votre vidéo une piste de son en sélectionnant un fichier de son.

Une fois tous ces paramètres ajustés, vous pouvez créer la vidéo en appuyant sur le bouton «Create Movie…», qui vous proposera un sélecteur de fichiers pour sauvegarder le fichier assemblé. Le temps de génération sera d’autant plus long que le nombre d’images de la séquence est important.


<iframe width="560" height="315" src="https://www.youtube.com/embed/ud1WQgQzFWU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>